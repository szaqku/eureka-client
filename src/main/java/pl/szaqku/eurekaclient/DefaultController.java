package pl.szaqku.eurekaclient;

import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/default")
@RequiredArgsConstructor
public class DefaultController {

    @Value("${spring.application.name}")
    String name;

    private final EurekaClient eurekaClient;

    @GetMapping("/hello")
    public ResponseEntity<String> defaultAnswer() {
        log.info("Hello");
        return ResponseEntity.ok("Hello there. From "+ eurekaClient.getApplication(name));
    }
}
